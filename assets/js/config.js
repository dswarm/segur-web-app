/* ============================================================
 * File: config.js
 * Configure routing
 * ============================================================ */

angular.module('app')
       .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$compileProvider',

              function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider , $compileProvider) {
                     $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|local|data|chrome-extension|teem):/);
                     $urlRouterProvider
                            .otherwise('/app/home');

                     $stateProvider
                            .state('app', {
                                   abstract: true,
                                   url: "/app",
                                   templateUrl: "tpl/app.html"
                            })
                            .state('app.validation', {
                                   url: "/validation",
                                   templateUrl: "tpl/validation.html",
                                   controller: "VerificationController"
                            })
                            .state('onboarding', {
                                   url: '/onboarding/:email/:userName',
                                   controller: 'OnboardingController',
                                   templateUrl: "tpl/onboarding.html",
                                    resolve: {
                                          deps: ['$ocLazyLoad', function($ocLazyLoad) {
                                                 return $ocLazyLoad.load([
                                                        'wizard'
                                                 ], {
                                                        insertBefore: '#lazyload_placeholder'
                                                 })
                                                        .then(function() {
                                                               return $ocLazyLoad.load('assets/js/controllers/onboarding.js');
                                                        });
                                          }]
                                   }
                            })
                            .state('migration', {
                                   url: '/migration/:netLocality/:netId/:netName',
                                   controller: 'MigrationController',
                                   templateUrl: "tpl/migration.html",
                                   resolve: {
                                          deps: ['$ocLazyLoad', function($ocLazyLoad) {
                                                 return $ocLazyLoad.load([
                                                        'wizard'
                                                 ], {
                                                        insertBefore: '#lazyload_placeholder'
                                                 })
                                                        .then(function() {
                                                               return $ocLazyLoad.load('assets/js/controllers/migration.js');
                                                        });
                                          }]
                                   }
                            })
                            .state('downloadapp', {
                                   url: "/ds",
                                   templateUrl: "tpl/download.html"
                            })
                            .state('privacy', {
                                   url: "/privacy-policy",
                                   templateUrl: "tpl/privacy.html"
                            })
                            .state('app.dashboard', {
                                   url: "/home",
                                   templateUrl: "tpl/home.html",
                                   controller: 'HomeController',
                                   resolve: {
                                          checkState: function (UserService, $state) {
                                                 UserService.checkState()
                                                        .then(function (response) {
                                                               if (response === null) {
                                                                      console.log(null);
                                                                      $state.go('access.login');
                                                               }
                                                        })
                                          },
                                          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                                 return $ocLazyLoad.load([
                                                        'datepicker',
                                                        'daterangepicker',
                                                        'timepicker',
                                                        'dropzone',
                                                        'skycons',
                                                        'moment',
                                                        'geofire',
                                                        'assets/js/services/UserService.js',
                                                        'assets/js/services/EventsService.js',
                                                        'assets/js/services/NetworkService.js'
                                                 ], {
                                                        insertBefore: '#lazyload_placeholder'
                                                 })
                                                        .then(function () {
                                                               return $ocLazyLoad.load([
                                                                      'assets/js/controllers/home.js'
                                                               ]);
                                                        });
                                          }]
                                   }
                            })
                            .state('app.network', {
                                   url: '/network/:locality/:networkId',
                                   templateUrl: 'tpl/network.html',
                                   controller: 'NetworkController',
                                   resolve: {
                                          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                                 return $ocLazyLoad.load([
                                                        'isotope',
                                                        'stepsForm'
                                                 ], {
                                                        insertBefore: '#lazyload_placeholder'
                                                 })
                                                        .then(function () {
                                                               return $ocLazyLoad.load([
                                                                      'tabcollapse',
                                                                      'pages/js/pages.social.min.js',
                                                                      'assets/js/apps/social/social.js',
                                                                      'assets/js/services/NetworkService.js'
                                                               ])
                                                        });
                                          }]
                                   }
                            })
                            .state('app.community', {
                                   url: '/community/:locality/:networkId',
                                   templateUrl: 'tpl/community.html',
                                   controller: 'CommunityController',
                                   resolve: {
                                          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                                 return $ocLazyLoad.load([
                                                        'isotope',
                                                        'stepsForm'
                                                 ], {
                                                        insertBefore: '#lazyload_placeholder'
                                                 })
                                                        .then(function () {
                                                               return $ocLazyLoad.load([
                                                                      'tabcollapse',
                                                                      'pages/js/pages.social.min.js',
                                                                      'assets/js/apps/social/social.js',
                                                                      'assets/js/services/NetworkService.js'
                                                               ])
                                                        });
                                          }]
                                   }
                            })
                            .state('app.settings', {
                                   url: "/settings",
                                   templateUrl: "tpl/settings.html",
                                   controller: 'SettingsController',
                                   resolve: {
                                          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                                 return $ocLazyLoad.load([
                                                        'tabcollapse',
                                                        'assets/js/services/UserService.js',
                                                        'assets/js/services/UploadService.js'
                                                 ], {
                                                        insertBefore: '#lazyload_placeholder'
                                                 })
                                                        .then(function () {
                                                               return $ocLazyLoad.load([
                                                                      'assets/js/controllers/settings.js'
                                                               ]);
                                                        });
                                          }]
                                   }
                            })
                         .state('app.maps', {
                             url: '/maps',
                             template: '<div class="full-height full-width" ui-view></div>'
                         })
                         .state('app.maps.google', {
                             url: '/google',
                             templateUrl: 'tpl/maps_google_map.html',
                             controller: 'GoogleMapCtrl',
                             resolve: {
                                 deps: ['$ocLazyLoad', function($ocLazyLoad) {
                                     return $ocLazyLoad.load([
                                         'google-map',
                                         'geofire',
                                         'moment',
                                         'assets/js/services/SwarmService.js'
                                     ], {
                                         insertBefore: '#lazyload_placeholder'
                                     })
                                         .then(function() {
                                             return $ocLazyLoad.load([
                                                 'assets/js/controllers/google_map.js',
                                                 'assets/plugins/AngularGeoFire/dist/angularGeoFire.js'
                                             ])
                                                 .then(function() {
                                                     return loadGoogleMaps();
                                                 });
                                         });
                                 }]
                             }
                         })
                            .state('access', {
                                   url: '/access',
                                   template: '<div class="full-height" ui-view></div>'
                            })
                            .state('access.login', {
                                   url: "/login",
                                   controller: 'LoginController',
                                   templateUrl: "tpl/login.html"
                            })

                     ;
              }
       ]);