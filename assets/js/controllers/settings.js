'use strict';

/* Controllers */

angular.module('app')

       .controller(

              'SettingsController'
              ,
              [
                '$scope'
               ,'$rootScope'
               ,'UserService'
               ,'UploadService'
                     , function(
                             $scope
                            ,$rootScope
                            ,UserService
                            ,UploadService
              ) {

              /***  --------------------- Variables ----------------------- */
              $scope.sendingEmail = false;
              $scope.emailSent = false;
              $scope.updatingName = false;
              $scope.uploadingAvatar = false;
              $scope.coordinatorAdded = false;
              $rootScope.superUser = false;

              if($rootScope.userId === '8Wcmgx2eIpVfzPB1FBk2HqyaoOA2'){
                     $rootScope.superUser = true;
              }


              /***  --------------------- Functions ----------------------- */
             /* var changeVar = function (user) {
                     $scope.updatingName = false;
                     $rootScope.userName = user.name;
              };*/

             $scope.addCoordinator = function (staff) {
                    $scope.coordinatorAdded = true;
                    UserService.addTeemStaff(staff)
                           .then(function () {
                                  console.log('success adding coordinator');
                                  $scope.coordinatorAdded = false;
                           })
             };

              $scope.accountLinkFB = function () {
                  UserService.linkAccount();
              };
              $scope.verify = function(){
                     $scope.sendingEmail = true;
                  UserService.verifyEmail()
                         .then(function (response) {
                             if(response){
                                $scope.sendingEmail = false;
                                $scope.emailSent = true;
                             } else {
                                $scope.sendingEmail = false;
                             }
                         })
              };
              $scope.updateUserName = function (user) {
                 $scope.updatingName = true;
                 UserService.updateName(user.name)
                        .then(function() {
                               // Update successful.
                               $scope.updatingName = false;
                               $rootScope.userName = user.name;
                               console.log('User Name succesfully updated to ' + user.name);
                        });
              };
              
              $scope.updateUserAvatar = function () {
                     $scope.uploadingAvatar = true;
                     var fileButton = document.getElementById('fileButton');
                     console.log($rootScope.userUid);
                     UploadService.uploadToFirebase(fileButton.files[0].name,fileButton.files[0], $rootScope.userUid,'avatars')
                            .then(function (response) {
                                   $scope.uploadingAvatar = false;
                                   UserService.updateAvatar(response);
                                   $rootScope.avatar = response;
                            });
              }
       }]);