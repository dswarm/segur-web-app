'use strict';

/* Controllers */

angular.module('app')
       .controller(
              'CommunityController'
              ,[
                     '$scope'
                     ,'$rootScope'
                     ,'$state'
                     ,'$timeout'
                     ,'UploadService'
                     ,'NetworkService'
                     ,
                     function(
                            $scope
                            , $rootScope
                            , $state
                            , $timeout
                            , UploadService
                            , NetworkService
                     ) {
                            $scope.uploadingLogo = false;
                            $scope.uploadingBgImage = false;
                            $scope.logoSelected = false;
                            $scope.bgImageSelected = false;
                            $scope.uploadingPostImage = false;
                            $scope.imagePostSelected = false;
                            $scope.imagePost = {};
                            $scope.imagePost.input = '';

                            /* $scope.modal = {};
                             $scope.modal.slideUp = "default";
                             $scope.modal.stickUp = "default";*/


                            console.log('NetworkController is running with the userUid: '+ $rootScope.userUid);

                            /*** Network Settings and methods to edit them
                             *
                             * @param netSettings
                             */

                            NetworkService.getSettings($state.params.locality, $state.params.networkId,'communities')
                                   .then(function (settings) {
                                          $scope.netSettings = settings;
                                          $timeout(function() {
                                                 $('[data-pages="parallax"]').parallax(); // initialize parallax
                                          });
                                   });
                            /***
                             * ImageProcessing
                             */

                            //----------------- Common functions ------------- \\

                            $scope.onUpdate = function(data){
                                   //$scope.outputLogoImage = data;
                                   console.log(data);
                            };
                            $scope.onUpdatePostImage = function(data){
                                   //$scope.outputLogoImage = data;
                                   console.log(data);
                            };

                            var uniqueId = function(imageType) {
                                   return imageType + Math.random().toString(36).substr(2, 16);
                            };

                            //----------------- Image Post ----------------\\

                            $scope.uploadPostImage = function (data) {
                                   console.log('uploadImagePost started');
                                   $scope.uploadingPostImage= true;
                                   console.log(data);
                                   $scope.imagePost = data;
                                   NetworkService.send(data, 'img')
                                          .then(function () {
                                                 $scope.imagePostSelected = false;
                                                 $scope.uploadingPostImage = false;
                                                 $('#imgPostButton').val('');
                                          });
                            };

                            var handleFileSelectImgPost=function(evt) {
                                   $scope.imagePostSelected = true;

                                   var file=evt.currentTarget.files[0];
                                   var reader = new FileReader();
                                   reader.onload = function (evt) {
                                          $scope.$apply(function($scope){
                                                 $scope.imagePost.input = evt.target.result;
                                          });
                                   };
                                   reader.readAsDataURL(file);
                                   //openLogoModal();
                            };

                            angular.element(document.querySelector('#imgPostButton')).on('change',handleFileSelectImgPost);


                            //----------------- Logo ----------------\\

                            $scope.uploadLogo = function (data) {
                                   console.log('uploadLogo started');
                                   var fileId = uniqueId('logo-');
                                   $scope.uploadingLogo= true;
                                   UploadService.uploadbase64Image($scope.logo.output, $rootScope.userUid, 'networkImages', fileId)
                                          .then(function (response) {
                                                 console.log('fileURL = '+response);
                                                 $scope.logoSelected = false;
                                                 $scope.uploadingBgImage = false;
                                                 NetworkService.setLogo($state.params.networkId, $state.params.locality, response, 'communities');
                                                 $scope.logo = response;
                                                 $scope.uploadingLogo = false;
                                                 $('#logoButton').val('');
                                          });
                            };

                            var handleFileSelectLogo=function(evt) {
                                   $scope.logoSelected = true;
                                   console.log('file Selected');
                                   console.log(uniqueId('logo-'));
                                   var file=evt.currentTarget.files[0];
                                   var reader = new FileReader();
                                   reader.onload = function (evt) {
                                          $scope.$apply(function($scope){
                                                 $scope.logo.input = evt.target.result;
                                          });
                                   };
                                   reader.readAsDataURL(file);
                                   //openLogoModal();
                            };

                            angular.element(document.querySelector('#logoButton')).on('change',handleFileSelectLogo);


                            //---------------------- bgImage ----------------------\\

                            $scope.uploadBgImage = function () {
                                   console.log('uploadBgImage Started');
                                   $scope.uploadingBgImage = true;
                                   var fileId = uniqueId('bgImage-');
                                   console.log($rootScope.userUid);
                                   UploadService.uploadbase64Image($scope.bgImage.output, $rootScope.userUid,'networkImages', fileId)
                                          .then(function (response) {
                                                 $scope.uploadingBgImage = false;
                                                 NetworkService.setBgImage($state.params.networkId, $state.params.locality, response,'communities');
                                                 $scope.bgImage = response;
                                                 $scope.bgImageSelected = false;
                                                 $('#bgImageButton').val('');
                                          });
                            };

                            var handleFileSelectbgImage=function(evt) {
                                   $scope.bgImageSelected = true;
                                   console.log('file Selected');
                                   var file=evt.currentTarget.files[0];
                                   var reader = new FileReader();
                                   reader.onload = function (evt) {
                                          $scope.$apply(function($scope){
                                                 $scope.bgImage.input = evt.target.result;
                                          });
                                   };
                                   reader.readAsDataURL(file);
                            };

                            angular.element(document.querySelector('#bgImageButton')).on('change',handleFileSelectbgImage);



                            /*** Other settings
                             *
                             * @param web
                             */

                            $scope.setWeb = function (web) {
                                   console.log('function running with--->' + web.link);
                                   NetworkService.updateWebsite($state.params.networkId, $state.params.locality, web.link, 'communities');
                                   document.getElementById('webpage').reset();
                            };

                            $scope.setPhone = function (phone) {
                                   NetworkService.updateTel($state.params.networkId, $state.params.locality, phone.number,'communities');
                                   document.getElementById('telephone').reset();
                            };

                            $scope.setDescription = function (description) {
                                   NetworkService.updateDescription($state.params.networkId, $state.params.locality, description.text, 'communities');
                                   document.getElementById('description').reset();
                            };

                            /*** Network Chat messages
                             *
                             * @param message
                             */

                            // Todo: When loading, posts the masonry format is not being applied  - Possible solutions here: http://mankindsoftware.github.io/angular-isotope/advanced.html
                            //
                            // Loads chats from the network firebase node.

                            NetworkService.selectRoom($state.params.locality, $state.params.networkId,'communities')
                                   .then(function (array) {
                                          $scope.messages = array;
                                          $timeout(function() {
                                                 $scope.$emit('iso-method', {name:null, params:null})
                                          });

                                   });

                            $scope.sendMessage = function (message) {
                                   console.log('Message from sendMessage function: '+message );
                                   NetworkService.send(message, 'txt');
                                   document.getElementById('updateForm').reset();
                                   //http://mankindsoftware.github.io/angular-isotope/advanced.html
                                   $scope.$emit('iso-method', {name:null, params:null})
                            };

                            $scope.uploadPicture = function () {
                                   var uploadCrop = $('#upload-demo').croppie({
                                          enableExif: true,
                                          viewport: {
                                                 width: 200,
                                                 height: 200,
                                                 type: 'circle'
                                          },
                                          boundary: {
                                                 width: 300,
                                                 height: 300
                                          }
                                   });
                            };
                            $scope.sendSet = function (djSet) {
                                   console.log(djSet.title + djSet.link);
                                   var title = djSet.title;
                                   var author =  djSet.author;
                                   var link = djSet.link;
                                   NetworkService.sendAudio(title, author, link, 'audio');
                                   document.getElementById('djSet').reset();
                                   //http://mankindsoftware.github.io/angular-isotope/advanced.html
                                   $scope.$emit('iso-method', {name:null, params:null})
                            };

                            $scope.remove = function (chat) {
                                   NetworkService.remove(chat);
                            };
                     }]);

angular.module('app')
       .directive('pgSocial', function() {
              return {
                     restrict: 'A',
                     link: function(scope, element, attrs) {
                            var $social = $(element);
                            $social.social($social.data());
                     }
              }
       });
