'use strict';

/* Controllers */

angular.module('app')
       .controller(
              'OnboardingController'
              ,[
                     '$scope'
                     ,'$state'
                     ,'UserService'
                     ,'NetworkService'
                     ,'InviteService'
                     ,'WizardHandler'
                     ,
                     function(
                            $scope
                            , $state
                            , UserService
                            , NetworkService
                            , InviteService
                            , WizardHandler
                     ) {
                            console.log('OnboardingController is running');
                            /***
                             *  Variables
                             *
                             */

                            var email = $state.params.email;
                            var cleanEmail = email.replace(/\./g,"(dot)");
                            var invNode;
                            var lang;
                            $scope.invites = {};
                            $scope.email = email.replace(/\(dot\)/g, '.');
                            $scope.name = $state.params.userName;
                            $scope.language = 'undefined';



                            /***
                             * Functions
                             *
                             */



                            $scope.removeNetwork = function ($index) {
                                   $scope.invites.splice($index, 1);

                            };


                            $scope.goEnglish = function () {
                                   $scope.language = 'english';
                                   lang = 'en';
                                   checkInvites(cleanEmail);
                                   $scope.headerText1 = 'You have been invited to join:';
                                   $scope.subHeaderText1 = 'With teem your community is coordinating and establishing security protocols';
                                   $scope.theseNetworks = 'These are the communities that invited you to join:';
                                   $scope.secNet = 'Security Network';
                                   $scope.regTitle = 'Create your account and join!';
                                   $scope.nameReg = 'Name';
                                   $scope.passReg = 'Password';
                                   $scope.createAccount = 'Create Account';
                                   $scope.validEmail = 'Please enter a valid email';
                                   $scope.fieldRequired = 'This field is required';
                                   $scope.minFields = 'Password should have at least 6 characters';
                                   $scope.skip = 'Skip';
                                   $scope.back = 'Back';
                                   $scope.headerText2 = 'Congratulations!';
                                   $scope.subHeaderText2 = 'You have taken the first step and most crucial step in your security... Integrating with your community.';
                                   $scope.teemIntro = 'What is teem?';
                                   $scope.download = 'Download teem';
                                   $scope.downloadInstruction = 'And login to the account you just created, We have also sent you an email with more information about security networks.';
                                   $scope.nextSteps = 'Next Steps';
                                   $scope.first = '1st';
                                   $scope.login = 'And login...';
                                   $scope.second = '2nd';
                                   $scope.secondImage = 'assets/img/onboarding/uploadPicture.png';
                                   $scope.goP = 'go to your profile';
                                   $scope.upP = 'And upload your picture';
                                   $scope.third = '3rd';
                                   $scope.thirdImage = 'assets/img/onboarding/registerHome.png';
                                   $scope.goS = 'go to the security network';
                                   $scope.reH = 'And register your home';
                                   $scope.fourth = '4th';
                                   $scope.fourthImage = 'assets/img/onboarding/addFamily.png';
                                   $scope.goNetSet = 'go to network settings';
                                   $scope.addFamily = 'And add family members!';
                                   $scope.step = 'STEP';
                                   $scope.accessories = 'Accessories';
                                   $scope.accText = 'Enhance teem with the following accessories';
                                   $scope.triggers = 'Control Button';
                                   $scope.teemFlic = 'With this special production of the flic button you can trigger teem\'s swarm without taking your phone out. You can also automate some other very cool things.';
                                   $scope.cost = 'Price';
                                   $scope.signage = 'Security Sign';
                                   $scope.signText = 'Let everybody know your community is a part of teem.';
                                   $scope.noInvitesTitle = 'You don\'t have any pending invitations.';
                                   $scope.noInvitesText = 'But nothing stops you to participate in teem!';

                            };

                            $scope.goSpanish = function () {
                                   $scope.language = 'spanish';
                                   lang = 'es';
                                   checkInvites(cleanEmail);
                                   $scope.headerText1 = 'Te han invitado a unirte a:';
                                   $scope.subHeaderText1 = 'Con teem, tu comunidad está coordinando y estableciendo protocolos de seguridad';
                                   $scope.theseNetworks = 'Estas son las comunidades que te han extendido una invitación:';
                                   $scope.secNet = 'Red de Seguridad';
                                   $scope.regTitle = '¡Crea tu cuenta y únete!';
                                   $scope.nameReg = 'Nombre';
                                   $scope.passReg = 'Contraseña';
                                   $scope.createAccount = 'Crea cuenta';
                                   $scope.validEmail = 'Por favor usa un email válido';
                                   $scope.fieldRequired = 'Este campo es requerido';
                                   $scope.minFields = 'La contraseña debe tener un mínimo de 6 caracteres';
                                   $scope.skip = 'Saltar';
                                   $scope.back = 'Regresar';
                                   $scope.headerText2 = '¡Enhorabuena!';
                                   $scope.subHeaderText2 = 'Integrarte con tu comunidad es el primer paso para mejorar tu seguridad.';
                                   $scope.teemIntro = '¿Qué es teem?';
                                   $scope.download = 'Descarga teem';
                                   $scope.downloadInstruction = 'E inicia sesión con la cuenta que acabas de crear, también te hemos enviado un correo electrónico con algunas instrucciones e información sobre la red de seguridad';
                                   $scope.nextSteps = 'Siguientes Pasos';
                                   $scope.first = '1er';
                                   $scope.login = 'E inicia sesión';
                                   $scope.second = '2do';
                                   $scope.secondImage = 'assets/img/onboarding/subeFoto.png';
                                   $scope.goP = 've a tu perfil';
                                   $scope.upP = 'Y sube tu foto';
                                   $scope.third = '3er';
                                   $scope.thirdImage = 'assets/img/onboarding/registraCasa.png';
                                   $scope.goS = 've a tu red de seguridad';
                                   $scope.reH = 'Y registra tu hogar';
                                   $scope.fourth = '4º';
                                   $scope.fourthImage = 'assets/img/onboarding/anadeFamilia.png';
                                   $scope.goNetSet = 've a los ajustes de red';
                                   $scope.addFamily = '¡Y añade a tu familia!';
                                   $scope.step = 'PASO';
                                   $scope.accessories = 'Accesorios';
                                   $scope.accText = 'Mejora teem con estos accesorios';
                                   $scope.triggers = 'Botón de Control';
                                   $scope.teemFlic = 'Con este botón flic de producción especial de teem, puedes controlar la app y detonar un enjambre sin sacar tu teléfono. También puedes controlar y hacer otras cosas con este aparato.';
                                   $scope.cost = 'Precio';
                                   $scope.signage = 'Señalización';
                                   $scope.signText = 'Haz del conocimiento de todos que eres parte de la comunidad teem.';
                                   $scope.noInvitesTitle = 'No tienes invitaciones pendientes.';
                                   $scope.noInvitesText = '!Pero nada te impide en participar en teem!';
                            };

                            $scope.createUser = function (email, name, pass) {

                                   UserService.register(email,pass)
                                          .then(function (user) {
                                                 var fbId = user.uid;
                                                 console.log(fbId + '<-- New user id');
                                                 UserService.updateName(name)
                                                        .then(function () {
                                                               angular.forEach($scope.invites, function (value) {
                                                                      var emailData = {
                                                                             type: 'welcomeToNet',
                                                                             recipient: name,
                                                                             email : email,
                                                                             language : lang,
                                                                             networkName : value.name
                                                                      };
                                                                      InviteService.sendUserCommInvite(emailData)
                                                                             .then(function (response) {
                                                                                    console.log('response = ' + response)
                                                                             });
                                                                      NetworkService.registerMember(value,name,email,fbId);
                                                                      InviteService.deleteInvite(cleanEmail,value.networkId,value.adminRegisterKey,value.sender);
                                                               });
                                                               WizardHandler.wizard().goTo('Descarga la aplicación');
                                                        })
                                          })
                            };

                            var checkInvites = function (email) {
                                   $scope.inviteEmail = email;
                                   console.log($scope.inviteEmail);
                                   //var cleanEmail = email.replace(/\./g,"(dot)");
                                   InviteService.getUserInvites(email)
                                          .then(function (response) {
                                                 console.log(JSON.stringify(response));
                                                 if(response.length > 0){
                                                        $scope.hasNoInvites = false;
                                                        $scope.invites = response;
                                                        invNode = response;
                                                        WizardHandler.wizard().next();
                                                 } else if (response.length === 0){
                                                        WizardHandler.wizard().goTo('Descarga la aplicación');
                                                        $scope.hasNoInvites = true;
                                                 }
                                          })
                            };


                            $scope.finished = function() {

                            };

                            $scope.logStep = function() {
                                   console.log("Step continued");
                            };

                            $scope.goBack = function() {
                                   WizardHandler.wizard().goTo(0);
                            };

                            $scope.getCurrentStep = function(){
                                   return WizardHandler.wizard().currentStepNumber();
                            };
                            $scope.goToStep = function(step){
                                   WizardHandler.wizard().goTo(step);
                            };


                     }]);