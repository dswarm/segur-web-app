'use strict';

/* Controllers */

angular.module('app')
    .controller('HomeController'
           , [
                    '$rootScope'
                  , '$scope'
                  , '$state'
                  , '$timeout'
                  , '$firebaseArray'
                  , 'DashboardService'
                  , 'UserService'
                  , 'NetworkService'
                  ,
                  function(
                         $rootScope
                         , $scope
                         , $state
                         , $timeout
                         , $firebaseArrays
                         , DashboardService
                         , UserService
                         , NetworkService

                  ) {

            /***  --------------------- Variables ----------------------- */
                  $scope.message = "Indicadores de uso de la aplicación:";
                  $scope.client = "San Miguel de Allende"

                  $scope.localityAdmin = false;

                  var userInfo;
                  var localityId;
                  var totalSwarmsWeek;
                  var totalUsers;

            /***  --------------------- Functions ----------------------- */
                     // This method get's the userUId, location and Networks.
                     UserService.checkState()
                            .then(function (response) {
                                   $rootScope.userUid = response.uid;
                                   $rootScope.locality = $scope.client;
                                   $rootScope.localityId = $scope.client;
                                   $scope.localityAdmin = true;
                                   UserService.getLocality(response.uid)
                                          .then(function (value) {
                                                 userInfo = value.val();
                                                 console.log(userInfo, null, '\t');
                                                 localityId = $scope.client;
                                                 if(!$rootScope.userNetworks){
                                                        UserService.getUserNetworks($scope.userUid)
                                                               .then(function (array) {
                                                                      $rootScope.userNetworks = array;
                                                               })
                                                 }
                                          });
                     });

                     DashboardService.getEvents("San Miguel de Allende").$loaded().then(function (value) {
                            console.log("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ Getting Events");
                            console.log(value);
                     });



                     $rootScope.logout = function () {
                        UserService.logOut().then(function () {
                                  $state.go('access.login');
                               })
                     };

    }]);

angular.module('app')
       .directive('timepicker', function() {
              return {
                     restrict: 'A',
                     link: function(scope, elem, attrs) {
                            $(elem).timepicker().on('show.timepicker', function(e) {
                                   var widget = $('.bootstrap-timepicker-widget');
                                   widget.find('.glyphicon-chevron-up').removeClass().addClass('pg-arrow_maximize');
                                   widget.find('.glyphicon-chevron-down').removeClass().addClass('pg-arrow_minimize');
                            });
                     }
              }
       });