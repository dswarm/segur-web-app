'use strict';

/* Controllers */

angular.module('app')
       .controller(
       'LoginController'
       ,[
               '$scope'
              ,'$state'
              ,'UserService'
              ,
              function(
                       $scope
                     , $state
                     , UserService
              ) {
                     console.log('LoginController is running');
                     $scope.botonLogin = function (user) {
                            UserService.loginUser(user.username, user.password)
                                   .then(function (firebaseUser) {
                                          console.log(firebaseUser);
                                          $state.go('app.dashboard')
                                   })
                     };

              }]);