'use strict';

/* Controllers */

angular.module('app')

    .controller('SwarmController', [
        '$scope',
        '$rootScope',
        '$state',
        '$timeout',
        'reverseGeocode',
        'moment',
        'UserService',
        'SwarmService',
        '$q',
        function(
            $scope,
            $rootScope,
            $state,
            $timeout,
            reverseGeocode,
            moment,
            UserService,
            SwarmService,
            $q
        ) {

        console.log('SwarmController is running');

        //-------------- Initialization  --------------- \\


            let user = "";
            let avatar = "";
            let unwatch;
            let swarm = {};
            let swarmId = "";
              $rootScope.swarms = [];
              $rootScope.unswarms = [];
              $rootScope.markers = [];
              $rootScope.userNotifications = [];

              $rootScope.superUser = false;
              $rootScope.lat = 20.935089;
              $rootScope.lng =  -100.744145;

               SwarmService.getActiveSwarms("San Miguel de Allende",20.935089,-100.744145);


            //--------------  Functions -------------- \\\

            UserService.checkState()
                .then(function (data) {
                    user = data.uid;
                    avatar = data.photoURL;
                    $scope.me = user;
                });
            
            $scope.getSwarmFromNotification = function (notification) {
                SwarmService.getSwarm(notification.locality, notification.swarmId)
                    .then(function (snap) {
                        swarm = snap.val();
                        swarm.key = notification.swarmId;

                        SwarmService.geocode(swarm.latitude, swarm.longitude)
                            .then(function (res) {
                                swarm.address = res;
                                console.log(JSON.stringify(swarm, null, '\t'));
                                $scope.messagesAlerts = SwarmService.getSwarmChat(swarm.locality,swarm.key);
                                $scope.addressAlerts = swarm.address;
                                $scope.createdAtAlerts = swarm.createdAt;
                                $scope.focusSwarmIdAlerts = swarm.key;
                                $scope.bindingsAlerts = SwarmService.getSwarmBinding(swarm.locality, swarm.key);
                            })
                    })
            };





               $scope.getMessages = function (swarm) {
                   console.log(JSON.stringify(swarm, null, 2));
                $scope.messages = SwarmService.getSwarmChat(swarm.locality,swarm.key);
                swarmId = swarm.key;
                $scope.address = swarm.address;
                $scope.audios = swarm.audios;
                $scope.citizen = swarm.citizen;
                $scope.createdAt = swarm.createdAt;
                $scope.focusSwarmId = swarm.key;
                $scope.focusLocality = swarm.locality;
                $scope.bindings = SwarmService.getSwarmBinding(swarm.locality, swarm.key);
                SwarmService.getNearbyUsers(swarm.latitude, swarm.longitude)
                    .then(function () {
                        //$state.go('app.maps');
                    });

                unwatch = $scope.bindings.$watch(function () {
                       let position = $scope.bindings.swarmPos;
                       let index = $rootScope.markers.map((e)=>{return e.id}).indexOf(swarm.key);
                       console.log(' Index of Marker : >>>>>>>>>>>>>>>> '+ index);
                       console.log('<<<<<<<<<<<<<<<<<<<<<<<<<<< Bindings Updated >>>>>>>>>>>>>>>>>>>>>>>>>>><')
                       SwarmService.updateSwarmMap(position, index);
                })



            };

            $scope.addMessage = function(message){
                console.log('adding message : '+ message);
                if(message.includes("/")){

                       switch (message){
                              case "/confirmado" : SwarmService.updateSwarmBindings(swarmId, {"confirmed": true});
                              break;
                              case "/noconfirmado" : SwarmService.updateSwarmBindings(swarmId, {"confirmed": false});
                              break;
                              case "/cerrar" : $scope.deActivateSwarm(swarmId, $rootScope.localityId);
                              break;
                              case "/inicio" : SwarmService.updateSwarm(swarmId, {"status": "start"});
                              break;
                              case "/encamino" : SwarmService.updateSwarm(swarmId, {"status": "dispatched"});
                              break;
                              case "/llego" : SwarmService.updateSwarm(swarmId, {"status": "arrived"});
                              break;
                              case "/folio" : {
                                     const array = message.split(" ");
                                     console.log(array[1]);
                              };
                              break;
                              case "/resetTracking" : SwarmService.resetTracking();
                              break;

                       }
                       document.getElementById('msg-input').reset();
                       document.getElementById('alerts-msg-input').reset();
                } else {
                       const chatMessage = {
                              id: $scope.me,
                              text: message,
                              createdAt: firebase.database.ServerValue.TIMESTAMP,
                              user: {
                                     avatar: "https://firebasestorage.googleapis.com/v0/b/aqui-no-d98af.appspot.com/o/images%2Favatars%2FsegurIconsmallest.png?alt=media&token=c6d6dba8-9171-494f-98b9-a0f1daf65836",
                                     name : "C4 San Miguel de Allende",
                                     uid: $scope.me,
                              }
                       };
                       $scope.messages.$add(chatMessage)
                              .then(function (res) {
                                     console.log('success sending message '+ res);
                                     document.getElementById('msg-input').reset();
                                     document.getElementById('alerts-msg-input').reset();
                              })
                              .catch(function (err) {
                                     console.log('error : ' + err);
                              })
                }

            };

            $scope.deActivateSwarm = function (swarmId, swarmLocality) {
                   console.log(swarmId + "<--- Removing this Swarm Id In this locality ----->" + $scope.focusLocality);
                SwarmService.deactivateSwarm(swarmId, $scope.focusLocality)
                    .then(function (res) {
                           SwarmService.updateSwarmBindings(swarmId, {"active": false});
                        console.log('success deactivating swarm '+ res);
                    })
                    .catch(function (err) {
                        console.warn('error deactivating swarm: '+ err);
                    })
            };

            $scope.playAudio = function (id) {
                const audio = document.getElementById(id);
                audio.play();
            };

    }]);