'use strict';

/* Controllers */

angular.module('app')
       .controller(
              'MigrationController'
              ,[
                     '$scope'
                     ,'$state'
                     ,'UserService'
                     ,'NetworkService'
                     ,'InviteService'
                     ,'WizardHandler'
                     ,
                     function(
                            $scope
                            , $state
                            , UserService
                            , NetworkService
                            , InviteService
                            , WizardHandler
                     ) {
                            console.log('MigrationController is running');
                            /***
                             *  Variables
                             *
                             */

                            var locality = $state.params.netLocality;
                            //var encodedLocality = encodeURIComponent(locality)
                            var netId = $state.params.netId;
                            $scope.teemLink = "teem://sidemenu.community/"+netId+"/"+locality;

                            $scope.invites = {};
                            $scope.netName = decodeURIComponent($state.params.netName);
                            $scope.language = 'undefined';

                            console.log(locality, netId, $scope.netName);


                            /***
                             * Functions
                             *
                             */
                            $scope.logo = "https://app.segurapp.io/pages/ico/120.png";

                            /*NetworkService.getSettings(locality,netId,"communities")
                                   .then(function (res) {
                                      console.log(res);
                                      netData = res;
                                      if(res.logo){
                                             $scope.logo = res.logo;
                                      } else {
                                             $scope.logo = "https://app.teem.is/pages/ico/76.png"
                                      }
                                   });*/



                     }]);