'use strict';

/* Controllers */

angular.module('app')
       .controller(
              'VerificationController'
              ,[
                     '$scope'
                     ,'$state'
                     ,'UserService'
                     ,
                     function(
                            $scope
                            , $state
                            , UserService
                     ) {
                            console.log('VerificationController is running');
                            UserService.getRFV().$loaded().then(function (users) {
                                   $scope.users = users;
                            });

                            $scope.setUser = function(user){
                                   console.log(user.$id);
                                   $scope.selUser = user.$id;
                            };
                            $scope.processVer = function (data){
                                   const citizenData = {
                                          "gender" : data.gender,
                                          "id" : data.id,
                                          "idNumber" : data.idNumber,
                                          "realName" : data.name,
                                   };
                                   console.log(citizenData);
                                   UserService.verifyUser($scope.selUser, citizenData).then(function(){
                                          $scope.selUser = null;
                                          document.getElementById('form-work').reset();
                                   }).catch(function (e) {
                                          console.warn(e);
                                   })


                            }
                     }]);