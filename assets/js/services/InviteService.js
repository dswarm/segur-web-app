angular.module('app').factory(

  'InviteService'

  , [
      '$http'
    , '$rootScope'
    , '$q'
    , '$firebaseArray'
    ,

    function(
        $http
      , $rootScope
      , $q
      , $firebaseArray
    )
    {


    var inviteWebHook = 'https://us-central1-teem-1071.cloudfunctions.net/sendInvites';

    var self = {
      sendUserCommInvite: function(data) {
             console.log(JSON.stringify(data));
        var d = $q.defer();
        $http({
               method: 'POST',
               url: inviteWebHook,
               data: data,
               headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
               console.log(response);
               if(response.status === 200){
                      d.resolve('success')
               } else { d.resolve('error')}
        });

        return d.promise
      },

      registerInvite : function (email, networkId, data, isAdmin, ref, isStaff) {
        var invitesRef = $rootScope.rootRef.child('invites/'+ email);
        if(isAdmin){
          data.isAdmin = true;
          data.adminRegisterKey = ref;
          data.networkId = networkId;
          data.sender = localStorage.getItem('fbId');
          invitesRef.child(networkId).set(data);
        } if(isStaff){
          data.isStaff = true;
          data.networkId = networkId;
          data.adminRegisterKey = ref;
          data.sender = localStorage.getItem('fbId');
          invitesRef.child(networkId).set(data);
        } else {
          data.adminRegisterKey = ref;
          data.sender = localStorage.getItem('fbId');
          data.networkId = networkId;
          invitesRef.child(networkId).set(data);
        }
      },
      getAdminInvites : function (networkId) {
        return $firebaseArray($rootScope.rootRef.child('invites/'+localStorage.getItem('fbId')+'/'+networkId)).$loaded()
      },

      deleteInvite : function (email, networkId, registerKey, sender) {
        var invitesRef = $rootScope.rootRef.child('invites/'+email+'/'+networkId);
        var adminRef = $rootScope.rootRef.child('invites').child(sender).child(networkId).child(registerKey);
        invitesRef.remove();
        adminRef.remove();
      },

      getUserInvites:function(userEmail){
        var userNetworksNode = $rootScope.rootRef.child('invites').child(userEmail);
        var d = $q.defer();
        var userNetworks = [];
        userNetworksNode.once('value').then(function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            userNetworks.push(childSnapshot.val())
          });
          d.resolve(userNetworks);
        });
        return d.promise;
      }

    };
    return self
    }
  ]
);
