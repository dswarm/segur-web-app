
'use strict';

angular.module('app')
       .factory(
              'UploadService'
              , [
                      '$q'
                     ,
                     function (
                             $q
                     ) {

                            console.log('UploadService is running');
                            var storageRef = firebase.storage().ref();

                            var self = {

                                   uploadToFirebase : function (fileName, file, userUid, subnode) {
                                          var d = $q.defer();

                                          var uploadTask = storageRef.child('users/'+userUid+'/'+subnode+'/'+fileName).put(file);
                                          // Register three observers:
                                          // 1. 'state_changed' observer, called any time the state changes
                                          // 2. Error observer, called on failure
                                          // 3. Completion observer, called on successful completion
                                          uploadTask.on('state_changed', function(snapshot){
                                                 // Observe state change events such as progress, pause, and resume
                                                 // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                                                 var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                                                 console.log('Upload is ' + progress + '% done');
                                                 switch (snapshot.state) {
                                                        case firebase.storage.TaskState.PAUSED: // or 'paused'
                                                               console.log('Upload is paused');
                                                               break;
                                                        case firebase.storage.TaskState.RUNNING: // or 'running'
                                                               console.log('Upload is running');
                                                               break;
                                                 }
                                          }, function(error) {
                                                 console.error(error);
                                                 // Handle unsuccessful uploads
                                          }, function() {
                                                 // Handle successful uploads on complete
                                                 // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                                                 var downloadURL = uploadTask.snapshot.downloadURL;
                                                 d.resolve(downloadURL);

                                          });
                                          return d.promise;
                                   },

                                   uploadbase64Image : function (base64, userUid, subnode,filename) {
                                          var d = $q.defer();

                                          //base64 = base64.replace(/\s/g, '');
                                          var block = base64.split(";");
                                          /** Firebase wont allow saving base64 files with the data:base64... imagetype set in so we extract those elements from the string
                                           *  source : http://ourcodeworld.com/articles/read/150/how-to-create-an-image-file-from-a-base64-string-on-the-device-with-cordova
                                           */
                                                 // Get the content type
                                          var dataType = block[0].split(":")[1];// In this case "image/jpg"
                                          var imageType = dataType.split("/")[1];
                                          // get the real base64 content of the file
                                          var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."
                                          var uploadTask = storageRef.child('users/'+userUid+'/'+subnode+'/'+filename+'.'+imageType).putString(realData,'base64', {contentType: dataType});
                                          // Register three observers:
                                          // 1. 'state_changed' observer, called any time the state changes
                                          // 2. Error observer, called on failure
                                          // 3. Completion observer, called on successful completion
                                          uploadTask.on('state_changed', function(snapshot){
                                                 // Observe state change events such as progress, pause, and resume
                                                 // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                                                 var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                                                 console.log('Upload is ' + progress + '% done');
                                                 switch (snapshot.state) {
                                                        case firebase.storage.TaskState.PAUSED: // or 'paused'
                                                               console.log('Upload is paused');
                                                               break;
                                                        case firebase.storage.TaskState.RUNNING: // or 'running'
                                                               console.log('Upload is running');
                                                               break;
                                                 }
                                          }, function(error) {
                                                 console.error(error);
                                                 // Handle unsuccessful uploads
                                          }, function() {
                                                 // Handle successful uploads on complete
                                                 // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                                                 var downloadURL = uploadTask.snapshot.downloadURL;
                                                 d.resolve(downloadURL);

                                          });
                                          return d.promise;
                                   }
                            };
                            return self;
                     }
              ]);
