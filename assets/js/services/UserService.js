'use strict';

angular.module('app')
       .factory(
              'UserService'
              , [
                      '$firebaseAuth'
                     ,'$firebaseArray'
                     , '$q'
                     , '$rootScope'
                     ,
                     function (
                             $firebaseAuth
                            ,$firebaseArray
                            , $q
                            , $rootScope
                     ) {

                           console.log('UserService is running');


                            var self = {

                               loginUser: function (email, pass) {
                                   var auth = $firebaseAuth();
                                   return auth.$signInWithEmailAndPassword(email, pass);
                                },

                                register:function (email, password) {
                                   var auth = $firebaseAuth();
                                   return auth.$createUserWithEmailAndPassword(email,password);
                                },

                                loginFB: function () {
                                   var auth = $firebaseAuth();
                                   var provider = new firebase.auth.FacebookAuthProvider();
                                   return auth.$signInWithPopup(provider)

                                },

                                getUser: function () {
                                  var teemUser = firebase.auth().currentUser;
                                  return teemUser;
                                },

                                verifyEmail:function () {
                                       var d = $q.defer();
                                       var teemUser = firebase.auth().currentUser;
                                       teemUser.sendEmailVerification().then(function() {
                                              console.log('email Sent');
                                              d.resolve(true);
                                       }, function(error) {
                                              d.resolve(false);
                                              console.log(error);
                                       });
                                       return d.promise;
                                },

                                linkAccount: function () {
                                       var authProvider = new firebase.auth.FacebookAuthProvider();
                                       firebase.auth().currentUser.linkWithRedirect(authProvider);
                                       firebase.auth().getRedirectResult().then(function(result) {
                                              if (result.credential) {
                                                     // Accounts successfully linked.
                                                     var credential = result.credential;
                                                     console.log(credential);
                                                     var user = result.user;
                                                     console.log(user);
                                                     // ...
                                              }
                                       }).catch(function(error) {
                                              console.error(error);
                                       });
                                },

                                updateName : function (name) {
                                      var user = firebase.auth().currentUser;
                                      var d = $q.defer();
                                       user.updateProfile({displayName: name})
                                              .then(function () {
                                                 d.resolve()
                                              }, function (error) {
                                                 console.error(error);
                                                 d.reject(error)
                                              });
                                      return d.promise;
                                },
                                updateAvatar : function (avatar) {
                                          var user = firebase.auth().currentUser;
                                          var d = $q.defer();
                                          user.updateProfile({photoURL: avatar})
                                                 .then(function () {
                                                        d.resolve()
                                                 }, function (error) {
                                                        console.error(error);
                                                        d.resolve()
                                                 });
                                          return d.promise;
                                   },

                               checkState : function () {
                                   var auth = $firebaseAuth();
                                   var d = $q.defer();
                                   auth.$onAuthStateChanged(function (authData) {
                                    if(authData){
                                      d.resolve(authData);
                                    } else {
                                      d.resolve(null);
                                    }
                                   });
                                    return d.promise;
                                },

                               logOut:function () {
                                   var auth = $firebaseAuth();
                                   return auth.$signOut();
                                },

                               getLocality:function (userId) {
                                  return $rootScope.rootRef.child('users/'+userId).once('value');
                               },

                               getRFV:function () {
                                      const  invitesRef = $rootScope.rootRef.child('validations')
                                      return $firebaseArray(invitesRef);

                               },

                               verifyUser : function(userId, data){
                                   const userRef = $rootScope.rootRef.child("users").child(userId);
                                   const  invitesRef = $rootScope.rootRef.child('validations')
                                   invitesRef.child(userId).remove();
                                   userRef.update(data);
                                   return userRef.child("pScore").update({"validated": true});
                               },
                                   /*** Index of User's networks
                                    *
                                    * @param userId
                                    * @returns {d.promise}
                                    */
                                   getUserNetworks:function(userId){
                                          var userNetworksNode = $rootScope.rootRef.child('users').child(userId).child('networks');
                                          var d = $q.defer();
                                          var userNetworks = [];
                                          userNetworksNode.once('value').then(function (snapshot) {
                                                 snapshot.forEach(function (childSnapshot) {
                                                        userNetworks.push({
                                                               networkId : childSnapshot.val().networkId,
                                                               name      : childSnapshot.val().name,
                                                               type      : childSnapshot.val().type,
                                                               latitude  : childSnapshot.val().latitude,
                                                               longitude : childSnapshot.val().longitude,
                                                               bgImage   : childSnapshot.val().bgImage,
                                                               logo      : childSnapshot.val().logo,
                                                               locality  : childSnapshot.val().locality,
                                                               settings   : childSnapshot.val().settings
                                                        })
                                                 });
                                                 d.resolve(userNetworks);
                                          });
                                          return d.promise;
                                   },
                                   getUserCommunities:function (userUid) {
                                          var communitiesRef = $rootScope.rootRef.child('users/'+userUid+'/communities')
                                          return communitiesRef.once('value');
                                   },

                                   addTeemStaff : function (staff) {
                                          var staffRef = $rootScope.rootRef.child('coordinators/'+staff.id);
                                          return staffRef.set(staff);
                                   },

                                    getUserNotifications : function (userId) {
                                            return $rootScope.rootRef.child('users').child(userId).child('notifications');
                                    }

                            };
                            return self;
                     }
              ]);
