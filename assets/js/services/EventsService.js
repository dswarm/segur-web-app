
'use strict';

angular.module('app')
       .factory(
              'DashboardService'
              , [
                     '$firebaseArray'
                     , '$q'
                     , '$rootScope'
                     ,
                     function (
                            $firebaseArray
                            , $q
                            , $rootScope
                     ) {

                            console.log('Events Service is running');
                            var eventRef = $rootScope.rootRef.child('swarms');
                            var self = {
                                   getEvents: function (locality) {
                                          var eventsArray = [];
                                            return eventsArray = $firebaseArray(eventRef.child(locality))
                                   },
                                   createEvent : function (locality, data) {
                                          eventRef.child(locality).push(data);
                                   }
                            };
                            return self;
                     }
              ]);
