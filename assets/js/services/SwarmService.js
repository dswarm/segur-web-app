
'use strict';

angular.module('app')
    .factory(
        'SwarmService'
        , [
            '$firebaseArray'
            , '$firebaseObject'
            , '$q'
            , '$rootScope'
            , '$timeout'
            , 'reverseGeocode'
            ,
            function (
                $firebaseArray
                , $firebaseObject
                , $q
                , $rootScope
                , $timeout
                , reverseGeocode
            ) {

                console.log('Swarm Service is running');


                const audio = new Audio('assets/sounds/sos.mp3');

                const showNot = function (message) {
                       audio.play().catch(function (err) {
                              console.log('err '+ err);
                       });
                       $('body').pgNotification({
                              style: 'circle',
                              title: 'Emergencia en:',
                              message: message,
                              position: 'top',
                              timeout: 0,
                              type: 'danger',
                              thumbnail: '<img width="40" height="40" style="display: inline-block;" src="assets/img/profiles/Alert2x.jpg" data-src="assets/img/profiles/Alert.jpg" ui-jq="unveil" data-src-retina="assets/img/profiles/Alert2x.jpg" alt="">'
                       }).show();
                };

                const swarmsRef = $rootScope.rootRef.child('swarms');

                const geocode = function (lat, lng) {
                    let d = $q.defer();
                    reverseGeocode.geocodePosition(lat, lng, function (address) {
                        console.log(address + '<--------- address from geocode');
                        d.resolve(address)
                    });
                    return d.promise
                };
                
                const self = {
                    geocode : function (lat, lng) {
                        let d = $q.defer();
                        reverseGeocode.geocodePosition(lat, lng, function (address) {
                            console.log(address + '<--------- address from geocode');
                            d.resolve(address)
                        });
                        return d.promise
                    },

                    getSwarm : function (locality, swarmId) {
                        return swarmsRef.child(locality).child(swarmId).once('value');
                    },


                    deactivateSwarm : function (swarmId) {
                           $rootScope.rootRef.child("swarmTriggers").child(swarmId).remove();
                           const $geo = new GeoFire($rootScope.rootRef.child('swarmGeoQ/'+$rootScope.localityId));
                           return $geo.remove(swarmId)
                    },
                    getSwarmChat : function (locality, swarmId) {
                        return $firebaseArray(swarmsRef.child(locality).child(swarmId).child('messages'))
                    },

                    resetTracking :function(){
                      return  $rootScope.rootRef.child('swarmsTracking').child("-MFrfxU2qn5pTatJo3JT").remove();
                    },
                    getActiveSwarms : function (locality, lat, lng) {
                        const $geo = new GeoFire($rootScope.rootRef.child('swarmGeoQ').child(locality));
                        const query = $geo.query({
                            center: [lat, lng],
                            radius: 5000
                        });
                        const activeSwarms = query.on("key_entered", function (key, location, distance) {
                            console.log(key + " entered query at " + location + " (" + distance + " km from center)");

                            self.getSwarm(locality, key)
                                .then(function (snap) {
                                    const swarm = snap.val();
                                    console.log(swarm);
                                       swarm.key = key;
                                           geocode(swarm.latitude, swarm.longitude)
                                                  .then(function (address) {
                                                         swarm.address = address;
                                                         console.log (swarm.address);
                                                         if(swarm.citizen.verified){
                                                                showNot(address);
                                                                $rootScope.swarms.push(swarm);

                                                          } else {
                                                                $rootScope.unswarms.push(swarm);
                                                                $timeout(function() {
                                                                       $rootScope.$apply();
                                                                });
                                                         }
                                                         const createdAt = moment(swarm.createdAt).fromNow();
                                                         const reason = swarm.bindings.reason;
                                                         const infoWindow = '<div id="content">'+
                                                                '<div id="siteNotice" class="card share" style="border: none!important;">'+
                                                                '<div class="card-header clearfix">'+
                                                                '<h5 id="firstHeading" class="firstHeading">Incidente en progreso</h5>'+
                                                                '<h6 id="firstHeading" class="firstHeading">'+swarm.address+'</h6>'+
                                                                '</div>'+
                                                                '<div id="bodyContent" class="card-description">'+
                                                                '<p>Creado: <span>'+createdAt+'</span>' +
                                                                '. Razón:  <b>'+reason+'</b>'+
                                                                '</div>'+
                                                                '</div>';
                                                         $rootScope.markers.push({
                                                                id : key,
                                                                latitude: parseFloat(location[0]),
                                                                longitude: parseFloat(location[1]),
                                                                icon: 'assets/img/icons/Alert.svg',
                                                                infoWindowContent: infoWindow,
                                                                label: 'Incidente activo',
                                                                url : 'url',
                                                                thumbnail : 'thumbnail',
                                                                createdAt: createdAt,
                                                                reason: reason
                                                         });
                                                         $timeout(function() {
                                                                $rootScope.$apply();
                                                         });
                                                  })

                                });
                        });

                        const inactiveSwarm = query.on('key_exited', function (key) {
                            console.log('key exited '+ key );
                            //$scope.markers = filterSwarm($scope.markers, key);
                            let newMarkers = $rootScope.markers.filter(function (marker) {
                                return marker.id !== key;
                            });
                            let newSwarms = $rootScope.swarms.filter(function (value) {
                                return value.key !== key;
                            });
                            $timeout(function() {
                                $rootScope.markers = newMarkers;
                                $rootScope.swarms = newSwarms;
                                $rootScope.$apply();
                            });

                        });
                    },
                    
                    getNearbyUsers :function (lat, lng) {
                        const d = $q.defer();
                        const $geo = new GeoFire($rootScope.rootRef.child('usersGeoQ'));
                        const query = $geo.query(
                            {
                                center : [lat,lng],
                                radius : .3
                            });

                        const nearbyUsers = query.on("key_entered", function (key, location, distance) {
                            const userId = key.split('*')[2];
                            const pushId = key.split('*')[1];
                            const infoWindow = '<div id="content">'+
                                '<div id="siteNotice" class="card share" style="border: none!important;">'+
                                '<div class="card-header clearfix">'+
                                '<h5 id="firstHeading" class="firstHeading">Usuario</h5>'+
                                '<h6 id="firstHeading" class="firstHeading">'+userId+'</h6>'+
                                '</div>'+
                                '</div>';
                            $rootScope.markers.push({
                                id : key,
                                latitude: parseFloat(location[0]),
                                longitude: parseFloat(location[1]),
                                icon: 'assets/img/icons/nearby.svg',
                                infoWindowContent: infoWindow
                            })
                        });
                        const exitedUser = query.on("key_exited", function (key) {
                        });
                        const queryReady = query.on("ready", function () {
                            d.resolve();
                        });
                        return d.promise;
                    },

                    getSwarmBinding : function (locality, swarmId) {
                        return $firebaseObject(swarmsRef.child(locality+'/'+swarmId+'/bindings'))
                    },

                   updateSwarmBindings : function(swarmId, data){
                     return swarmsRef.child($rootScope.localityId).child(swarmId).child("bindings").update(data);
                   },

                   updateSwarm: function(swarmId, data){
                     return swarmsRef.child($rootScope.localityId).child(swarmId).update(data);
                   },

                    updateSwarmMap: function (bindings,index) {
                           console.log(JSON.stringify(bindings, null, '\t'));
                           console.log($rootScope.markers);
                           $rootScope.markers[index].latitude = bindings.lat;
                           $rootScope.markers[index].longitude = bindings.lng;
                           this.getNearbyUsers(bindings.lat,bindings.lng);
                           $timeout(function() {
                                  $rootScope.$apply();
                           });
                    }
                };
                return self;
            }
        ]);
