
'use strict';

angular.module('app')
       .factory(
              'NetworkService'
              , [
                      '$rootScope'
                     ,'$firebaseArray'
                     ,'$q'
                     ,
                     function (
                             $rootScope
                            ,$firebaseArray
                            ,$q
                     ) {

                            console.log('NetworkService is running');
                            var chats;
                            var cart;

                            var self = {
                                   getCart :function (locality, networkId) {
                                          var d = $q.defer();
                                          var cartRef = $rootScope.rootRef.child('networks').child(locality).child(networkId).child('cart');
                                          cart = $firebaseArray(cartRef);
                                          d.resolve(cart);
                                          return d.promise;
                                   },

                                   /*** Update Network's details
                                    *
                                    * @param networkId
                                    * @param locality
                                    * @param fileURL, tel, description, website
                                    */

                                   setBgImage : function (networkId, locality, fileURL,node) {
                                          console.log('setBgImage is running with --->' + networkId + fileURL);
                                          if(node === 'networks'){
                                                 var indexNode = $rootScope.rootRef.child('networksIndex').child(locality).child(networkId);
                                                 indexNode.update({
                                                        bgImage: fileURL,
                                                        settings : {
                                                               premium : true,
                                                               vacancy : false,
                                                               messages : false
                                                        }
                                                 });

                                          }
                                          if (node === 'communities'){
                                                 var registrationPaths = {};
                                                 var membershipRef = $rootScope.rootRef.child('memberships');

                                                 membershipRef.child(networkId+'/members').once('value')
                                                        .then(function (snapshot) {
                                                               snapshot.forEach(function (childSnap) {
                                                                      console.log(childSnap.val());
                                                                      var fbId = childSnap.val().fbId;
                                                                      registrationPaths['users/'+fbId+'/communities/'+networkId+'/bgImage'] = fileURL;
                                                               });
                                                        $rootScope.rootRef.update(registrationPaths, function (error) {
                                                               if(error){ console.error(error) } else { console.log('successfully updated nodes!')}
                                                        });
                                                 })
                                          }
                                          var networkNode = $rootScope.rootRef.child(node).child(locality).child(networkId);
                                          networkNode.update(
                                                 {
                                                        bgImage: fileURL,
                                                        premium: true
                                                 });
                                   },

                                   setLogo : function (networkId, locality, fileURL, node) {
                                          console.log('setBgImage is running with --->' + networkId + fileURL);
                                          var networkNode = $rootScope.rootRef.child(node).child(locality).child(networkId);
                                          if(node === 'networks'){
                                                 var indexNode = $rootScope.rootRef.child('networksIndex').child(locality).child(networkId);
                                                 indexNode.update({
                                                        logo: fileURL,
                                                        settings : {
                                                               premium : true,
                                                               vacancy : false,
                                                               messages : false
                                                        }
                                                 });
                                                 networkNode.update(
                                                        {
                                                               logo: fileURL,
                                                               premium: true
                                                        });
                                          }
                                          if (node === 'communities'){
                                                 var registrationPaths = {};
                                                 var membershipRef = $rootScope.rootRef.child('memberships');

                                                 membershipRef.child(networkId+'/members').once('value')
                                                        .then(function (snapshot) {
                                                        snapshot.forEach(function (childSnap) {
                                                               console.log(childSnap.val());
                                                               var fbId = childSnap.val().fbId;
                                                               registrationPaths['users/'+fbId+'/communities/'+networkId+'/logo'] = fileURL;
                                                        });
                                                        $rootScope.rootRef.update(registrationPaths, function (error) {
                                                               if(error){ console.error(error) } else { console.log('successfully updated nodes!')}
                                                        })
                                                 });
                                                 networkNode.update(
                                                        {
                                                               logo: fileURL,
                                                               premium: true
                                                        });

                                          }
                                   },

                                   updateTel : function (networkId, locality, tel, node) {
                                          var networkNode = $rootScope.rootRef.child(node).child(locality).child(networkId);
                                          networkNode.update({tel:tel});
                                   },

                                   updateWebsite : function (networkId, locality, link,node) {
                                          var networkNode = $rootScope.rootRef.child(node).child(locality).child(networkId);
                                          networkNode.update({web:link});
                                   },

                                   updateDescription : function (networkId, locality, description, node) {
                                          var networkNode = $rootScope.rootRef.child(node).child(locality).child(networkId);
                                          networkNode.update({description:description});
                                   },

                                   updateCart : function (networkId, locality, data, node) {
                                          var networkNode = $rootScope.rootRef.child(node).child(locality).child(networkId).child('cart');
                                          networkNode.push(data);
                                   },

                                   removeProductFromCart: function (product) {
                                          cart.$remove(product)
                                                 .then(function () {
                                                        console.log('item removed succesfully')
                                                 })
                                   },

                                   updateCartDescription : function (networkId, locality, description) {
                                          var networkNode = $rootScope.rootRef.child('networks').child(locality).child(networkId);
                                          networkNode.update({cartDescription:description});
                                   },

                                   /***
                                    * Chat room functions
                                    * @returns {*}
                                    */
                                   all: function () {
                                          return chats;
                                   },

                                   remove: function (chat) {
                                          chats.$remove(chat)
                                                 .then(function (ref) {
                                                        // true item has been removed
                                                        //ref.key() == chat.$id;
                                                 });
                                   },

                                   get: function (chatId) {
                                          for (var i = 0; i < chats.length; i++) {
                                                 if (chats[i].id == parseInt(chatId)) {
                                                        return chats[i];
                                                 }
                                          }
                                          return null;
                                   },

                                   selectRoom: function (locality, networkId, node) {
                                          var d = $q.defer();
                                          console.log("selecting the room with id: " + networkId);
                                          var chatRef = $rootScope.rootRef.child(node).child(locality).child(networkId).child('chats');
                                          chats = $firebaseArray(chatRef);
                                          d.resolve(chats);
                                          return d.promise;
                                   },

                                   send: function (message, type) {
                                          var d = $q.defer();
                                          if(type == 'img'){

                                                 var img = message;
                                                 var block = img.split(";");
                                                 /** Firebase wont allow saving base64 files with the data:base64... imagetype set in so we extract those elements from the string
                                                  *  source : http://ourcodeworld.com/articles/read/150/how-to-create-an-image-file-from-a-base64-string-on-the-device-with-cordova
                                                  */
                                                 // get the real base64 content of the file
                                                 var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."
                                                 console.log(realData);
                                                 message = realData;
                                          }
                                          var chatMessage = {
                                                 from: $rootScope.userName,
                                                 avatar: $rootScope.avatar,
                                                 fbId : localStorage.getItem('fbId'),
                                                 message: message,
                                                 type: type,
                                                 createdAt: firebase.database.ServerValue.TIMESTAMP
                                          };
                                          chats.$add(chatMessage)
                                                 .then(function () {
                                                        console.log("added entry");
                                                        d.resolve();
                                                 });
                                          return d.promise;
                                   },

                                   sendAudio: function (title, author, message, type) {
                                          var chatMessage = {
                                                 from: $rootScope.userName,
                                                 avatar: $rootScope.avatar,
                                                 fbId : localStorage.getItem('fbId'),
                                                 message: message,
                                                 type: type,
                                                 author: author,
                                                 title: title,
                                                 createdAt: firebase.database.ServerValue.TIMESTAMP
                                          };
                                          chats.$add(chatMessage)
                                                 .then(function () {
                                                        console.log("added entry");
                                                 });
                                   },

                                   getSettings: function (locality, networkId, node) {
                                          var ref = $rootScope.rootRef.child(node).child(locality).child(networkId);
                                          var d = $q.defer();
                                          var networkSettings ={};
                                          ref.once('value')
                                                 .then(function (snapshot) {
                                                        var networkParams = snapshot.val();
                                                               networkSettings.lat = networkParams.latitude;
                                                               networkSettings.lng = networkParams.longitude;
                                                               networkSettings.name = networkParams.name;
                                                               networkSettings.description = networkParams.description;
                                                               networkSettings.type = networkParams.type;
                                                               networkSettings.tel = networkParams.tel;
                                                               networkSettings.locality = networkParams.locality;
                                                               networkSettings.localityName = networkParams.localityName;
                                                               networkSettings.product = networkParams.product;
                                                               networkSettings.cartDescription = networkParams.cartDescription;
                                                        if (networkParams.premium == true){
                                                               networkSettings.networkPremium = true;
                                                               networkSettings.bgImage = networkParams.bgImage;
                                                               networkSettings.logo = networkParams.logo;
                                                               networkSettings.web = networkParams.web;
                                                        }
                                                        if (networkParams.googleId){
                                                               networkSettings.googleId = networkParams.googleId;
                                                        }

                                                        d.resolve(networkSettings);
                                                 });
                                          return d.promise
                                   },
                                   registerCommunityAdmin : function (networkId, locality, fbId, email) {
                                          console.log('registerCommunityAdmin is running!------------------------');
                                          return $rootScope.rootRef.child('communities/'+locality+'/'+ networkId +'/admin/'+fbId).set({email: email, isRegistered : false})
                                   },

                                   registerMember : function (input, name, email, fbId) {
                                          console.log(JSON.stringify(input));
                                          var admin = input.sender;
                                          var netKeyFB = input.networkId;
                                          var userComRef = $rootScope.rootRef.child('users/'+fbId+'/communities/');
                                          var adminRegister = $rootScope.rootRef.child('memberships/'+netKeyFB+'/'+admin);
                                          var membershipRef = $rootScope.rootRef.child('memberships');

                                          var userData = {
                                                 name: name,
                                                 email: email,
                                                 fbId: fbId
                                          };
                                          var data = {
                                                 name: input.name,
                                                 latitude: input.latitude,
                                                 longitude: input.longitude,
                                                 locality: input.locality,
                                                 description: input.description,
                                                 networkId: input.networkId,
                                                 type : 'community'
                                          };
                                          if(input.logo){
                                                 data.logo = input.logo;
                                          }
                                          if(input.bgImage){
                                                 data.bgImage = input.bgImage;
                                          }

                                          membershipRef.child(netKeyFB).child('members').child(fbId).update(userData);
                                          if(input.isAdmin === true){
                                                 console.log('user is Admin for this community');
                                                 this.registerCommunityAdmin(netKeyFB, data.locality, fbId, data, email)
                                                        .then(function () {
                                                               console.log('success registering admin')
                                                        })
                                          }
                                          userComRef.child(netKeyFB).set(data);
                                          adminRegister.push(userData);
                                   }
                            };
                            return self;
                     }
              ]);
