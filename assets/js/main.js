/* ============================================================
 * File: main.js
 * Main Controller to set global scope variables. 
 * ============================================================ */

angular.module('app')
    .controller(
           'AppCtrl'
           , [
                     '$scope'
                  ,  '$rootScope'
                  ,  '$state'
                  ,  '$timeout'
                  ,  'firebase'
                  ,  'UserService'
                  ,
                  function
                         (
                                   $scope
                                ,  $rootScope
                                ,  $state
                                ,  $timeout
                                ,  firebase
                                ,  UserService
                         )
                  {

        // App globals

           // Initialize Firebase

           var config = {
                  apiKey: "AIzaSyBf9hd4-7GaZlbwL38r1RvWfOGKRPpVBU0",
                  authDomain: "aqui-no-d98af.firebaseapp.com",
                  databaseURL: "https://aqui-no-d98af.firebaseio.com",
                  projectId: "aqui-no-d98af",
                  storageBucket: "aqui-no-d98af.appspot.com",
                  messagingSenderId: "838750626495",
                  appId: "1:838750626495:web:1c0e7a70165dc5dce3dc0a",
                  measurementId: "G-WW7DHS65SD"
           };

           firebase.initializeApp(config);
           $rootScope.rootRef = firebase.database().ref();
           $rootScope.storage = firebase.storage();
           console.log("root firebase ---->"+$rootScope.rootRef);
           console.log($rootScope.storage);


           // Get User Settings

           UserService.checkState().then(function (userData) {
                  console.log('UserService is running from main.js');
              $rootScope.userName = userData.displayName;
              $rootScope.user = userData;
              $rootScope.userId = userData.uid;
              $rootScope.emailVerified = userData.emailVerified;
                  if ($rootScope.user != null) {
                         console.log('User Verified email = '+$scope.user.emailVerified);
                         $scope.user.providerData.forEach(function (profile) {
                                console.log("  Sign-in provider: "+profile.providerId);
                                console.log("  Provider-specific UID: "+profile.uid);
                                console.log("  Name: "+$scope.user.displayName);
                                console.log("  Email: "+profile.email);
                                console.log("  Photo URL: "+$scope.user.photoURL);
                                $rootScope.avatar = $scope.user.photoURL;
                                console.log("  Email Verified:  "+$scope.user.emailVerified)
                         });
                  }
                  $timeout(function() {
                         $rootScope.$apply();
                  });
                  UserService.getUserNetworks(userData.uid)
                         .then(function (array) {
                                $rootScope.userNetworks = array;
                         });
                  UserService.getUserCommunities(userData.uid)
                         .then(function (array) {
                                $rootScope.userCommunities = array.val();
                                console.log($rootScope.userCommunities);
                                console.log('getUserComms is running');
                         });
                  UserService.getUserNotifications($rootScope.userId)
                    .on('value', function (snap) {
                       console.log('userNotifications' + snap.val());
                       $rootScope.userNotifications = snap.val();
                       console.log('userNotifications length' + snap.numChildren());
                       $rootScope.notificationsLength =  snap.numChildren();
                        $timeout(function() {
                            $rootScope.$apply();
                        });
                   });
           });

        $scope.app = {
            name: 'San Miguel de Allende Seguro',
            description: 'Secretaría de Seguridad Pública',
            layout: {
                menuPin: false,
                menuBehind: false,
                theme: 'pages/css/pages.css'
            },
            author: 'Sergio Iván Martell Arteaga'
        };

        // Checks if the given state is the current state
        $scope.is = function(name) {
            return $state.is(name);
        };

        // Checks if the given state/child states are present
        $scope.includes = function(name) {
            return $state.includes(name);
        };

        // Broadcasts a message to pgSearch directive to toggle search overlay
        $scope.showSearchOverlay = function() {
            $scope.$broadcast('toggleSearchOverlay', {
                show: true
            })
        };
    }]);


angular.module('app')
    /*
        Use this directive together with ng-include to include a 
        template file by replacing the placeholder element
    */
    
    .directive('includeReplace', function() {
        return {
            require: 'ngInclude',
            restrict: 'A',
            link: function(scope, el, attrs) {
                el.replaceWith(el.children());
            }
        };
    });