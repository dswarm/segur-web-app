/* ============================================================
 * File: app.js
 * Configure global module dependencies. Page specific modules
 * will be loaded on demand using ocLazyLoad
 * ============================================================ */
'use strict';

angular.module('app', [
    'firebase',
    'ui.router',
    'ui.utils',
    'oc.lazyLoad',
    'angularMoment',
    'angular-ladda',
       'ngCroppie',
    'angularGeoFire',
    'AngularReverseGeocode',
    'luegg.directives'
]);